#ifndef NX
#define NX 20
#endif
#ifndef DEBUG
#define DEBUG 0
#endif

module utility_m
contains

	subroutine linspace(from, to, array, endpoint)
	!thank you: https://stackoverflow.com/a/57211848
	!modified to add endpoint true/false support
	implicit none
    real(8), intent(in) :: from, to
    real(8), intent(out) :: array(:)
    logical, intent(in) :: endpoint
    real(8) :: range
    integer :: n, d, i

    n = size(array)
    if (endpoint) then 
    	d = -1
    else
    	d = 0
    end if 

    range = to - from

    if (n == 0) return

    if (n == 1) then
        array(1) = from
        return
    end if

    do i=1, n
        array(i) = from + range * (i - 1) / (n + d)
    end do
	end subroutine linspace

	subroutine eye(n, matrix)
	implicit none 
	integer, value, intent(in) :: n 
	real(8), intent(out) :: matrix(n, n)
	integer :: i

	matrix = 0.0
	do i=1,n 
		matrix(i, i) = 1.0
	end do 

	end subroutine eye

	subroutine printmatrix(b,n,m)
	!http://math.hawaii.edu/~gautier/math_190_lecture_11.pdf
	integer::n,m
	real(8)::b(n,m) !n = # rows, m = # columns
	do i=1,n; print '(20f6.2)',b(i,1:m); enddo
	end subroutine printmatrix

end module utility_m

program main
	use, intrinsic :: iso_fortran_env, only: qp=>real128, dp=>real64
	use utility_m
	use impmid_m
	implicit none 
	integer :: nt
	real(qp) :: l2_err, tmp(NX), u_ref(NX), start, finish
	real(dp) :: beta, ti, tf, xi, xf, dx, dt
	real(dp) :: xs(NX), uf(NX)
	real(dp), allocatable :: ts(:)
	character(len=32) :: arg
	character(len=32) :: fname
	character(len=32) :: tmp_char

	beta = 1.0
	ti = 0.0
	tf = 0.1
	xi = 0.0
	xf = 1.0 

	call linspace(xi, xf, xs, .TRUE.) !spacial grid contains endpoint
	dx = xs(2) - xs(1)

	call getarg(1, arg)
	read(arg, *)nt 
	allocate(ts(nt))
	call linspace(ti, tf, ts, .FALSE.) !times does not contain endpoint
	dt = ts(2) - ts(1)
	if (DEBUG == 1) then 
		!print *, 'ts = ', ts(:)
		print *, 'dt = ', dt
	end if 

	call cpu_time(start)
	uf = impmid_driver(nt, ts, xs, dx, dt, ti, tf, beta)

	write(tmp_char, "(I0.6)"), NX
	fname = "../ref_sol/ref_sol_"// trim(tmp_char) //".txt"

	open(unit=2, file=fname)
	read (2,*) u_ref(:) 
	close(2)

	tmp = real(uf, qp) - u_ref
	l2_err = sqrt(real(dx, qp) * dot_product(tmp, tmp))
	print *, "L2 Error with reference: ", l2_err

end program main