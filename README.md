
# Heat Equation Solver using cuBLAS

## 1D Heat equation

To run the 1D heat equation experiment, a python environment created with the following is recommended:

```
python -m venv mpark-env
. mpark-env/bin/activate
pip install numpy matplotlib pandas
```

An install of `nvfortran` is also necessary. Our tests were done with the `nvhpc` module provided by Nvidia.

The `error_experiment.py` can then be used to reproduce the experiment used to collect our data.

Individual tests can be run by first running the reference solution as follows:

```
cd 1dheat/ref_sol
make nx=50
./rk4.x 1000000 1 #nt = 1000000
```

This will create the reference solution that is used by all experiments. A method can then be tested and verified with the following

```
cd 1dheat/impmid
make imr nx=50
./impmid.x 1000 #nt = 1000
cd ..
python verify_method.py impmid 50
```

## 2D Heat Equation

_A work in progress_

