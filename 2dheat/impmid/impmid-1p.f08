!nvfortran -o impmid-1p.x impmid-1p.f08 -cpp -DNX=50 -cuda -cudalib=cublas -cudalib=cusolver

#ifndef NX
#define NX 20
#endif
#ifndef DEBUG
#define DEBUG 0
#endif
#ifndef FULL_TYPE
#define FULL_TYPE real64
#endif
#ifndef REDU_TYPE
#define REDU_TYPE real32
#endif

module utility_m
use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
contains

	subroutine linspace(from, to, array, endpoint)
	!thank you: https://stackoverflow.com/a/57211848
	!modified to add endpoint true/false support
	implicit none
    real(fp), intent(in) :: from, to
    real(fp), intent(out) :: array(:)
    logical, intent(in) :: endpoint
    real(fp) :: range
    integer :: n, d, i

    n = size(array)
    if (endpoint) then 
    	d = -1
    else
    	d = 0
    end if 

    range = to - from

    if (n == 0) return

    if (n == 1) then
        array(1) = from
        return
    end if

    do i=1, n
        array(i) = from + range * (i - 1) / (n + d)
    end do
	end subroutine linspace

	subroutine eye(n, matrix)
	implicit none 
	integer, value, intent(in) :: n 
	real(fp), intent(out) :: matrix(n, n)
	integer :: i

	matrix = 0.0_fp
	do i=1,n 
		matrix(i, i) = 1.0_fp
	end do 

	end subroutine eye

	subroutine eye_rp(n, matrix)
	implicit none 
	integer, value, intent(in) :: n 
	real(rp), intent(out) :: matrix(n, n)
	integer :: i

	matrix = 0.0_rp
	do i=1,n 
		matrix(i, i) = 1.0_rp
	end do 

	end subroutine eye_rp

	subroutine printmatrix(b,n,m)
	!http://math.hawaii.edu/~gautier/math_190_lecture_11.pdf
	integer::n,m
	real(8)::b(n,m) !n = # rows, m = # columns
	do i=1,n; print '(20f6.2)',b(i,1:m); enddo
	end subroutine printmatrix

end module utility_m

module impmid_m
use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
contains	

	subroutine exact_solution(x, t, beta, uf)
	implicit none
	real(fp), intent(in) :: x(NX), t, beta
	real(fp), intent(out) :: uf(NX)
	real(fp) :: pi = 4.0_fp*atan(1.0_fp) !supposedly this is pi: https://stackoverflow.com/questions/2157920/why-define-pi-4atan1-d0
	integer :: i 

	do i=1,NX
		uf(i) = exp(-4.0_fp * beta * t * pi) * sin(2.0_fp * pi * x(i))
	end do

	end subroutine exact_solution

	subroutine init_u(u, x)
	implicit none 
	real(fp), intent(inout) :: u(NX)
	real(fp), intent(in) :: x(NX)
	integer :: i
	real(fp) :: pi = 4.0_fp*atan(1.0_fp) !supposedly this is pi: https://stackoverflow.com/questions/2157920/why-define-pi-4atan1-d0
	!real(8) :: pi = 3.14159
	u = 0.0_fp
	do i=1,NX-1 
		u(i) = sin(2.0_fp * pi * x(i))
	end do 

	end subroutine init_u

	subroutine init_a(a, gamma)
	implicit none 
	real(fp), value, intent(in) :: gamma
	real(fp), intent(inout) :: a(NX, NX)
	integer :: i 

	a = 0.0_fp
	do i=2,NX-1
		a(i, i+1) = gamma 
		a(i, i) = -2.0_fp * gamma 
		a(i, i-1) = gamma 
	end do
	a(1, 1) = 1.0_fp
	a(NX, NX) = 1.0_fp

	end subroutine init_a

	subroutine init_b(a, b)
	use utility_m
	implicit none
	real(fp), intent(in) :: a(NX, NX)
	real(rp), intent(inout) :: b(NX, NX)

	call eye_rp(NX, b)
	b = b - real(a, rp) / 2.0_rp

	end subroutine init_b
	
	function impmid_driver(nt, ts, xs, dx, init_dt, ti, tf, beta) result(u)
	!https://docs.nvidia.com/hpc-sdk/compilers/fortran-cuda-interfaces/index.html#iface-introduction
	use cudafor 
	use cublas
	use cusolverDn 
	use utility_m
	implicit none 
	integer, value, intent(in) :: nt 
	real(fp), intent(in) :: ts(:), xs(NX), dx, init_dt, ti, tf, beta
	real(fp) :: t_f, dt, t, gamma
	real(fp) :: a(NX, NX), u(NX) 
	real(rp) :: b(NX, NX)
	integer :: i, j, k, stat, n_corr=1
	real(fp), device :: a_d(NX, NX), u_d(NX), yk_d(NX), tmp_d(NX)
	real(rp), device :: b_d(NX, NX), y1_d(NX), workspace_d(NX)
	integer, device :: pivots_d(NX), info_d
	type (cublasHandle) ::cb_h
	type (cusolverDnHandle) :: cs_h
	stat = cublasCreate(cb_h)
	stat = cusolverDnCreate(cs_h)

	!initializations
	dt = init_dt
	gamma = beta * dt / (dx * dx)
	call init_u(u, xs)
	u_d = u 
	
	!initialize the matricies for LU. Only perform one LU decompose on b 
	!then use the getrs solvers on decomposed b each iteration.
	call init_a(a, gamma)
	a_d = a 
	call init_b(a, b)
	b_d = b

	!https://docs.nvidia.com/cuda/cusolver/index.html#cuSolverDN-lt-t-gt-getrf
	stat = cusolverDnSgetrf(cs_h, NX, NX, b_d, NX, workspace_d, pivots_d, info_d)

	t_f = ti 
	!run time stepping loop
	do i=1,size(ts)
		!implicit step
		!https://docs.nvidia.com/cuda/cusolver/index.html#cuSolverDN-lt-t-gt-getrs
		!$cuf kernel do <<< *, * >>> 
		do j=1,NX
			y1_d(j) = real(u_d(j), rp)
		end do 
		!Note I have no idea what nrhs is supposed to be so I just set it to 1
		stat = cusolverDnSgetrs(cs_h, CUBLAS_OP_N, NX, 1, b_d, NX, pivots_d, y1_d, NX, info_d)
		!correction step
		!$cuf kernel do <<< *, * >>> 
		do j=1,NX
			yk_d(j) = real(y1_d(j), fp)
		end do
		do j=1,n_corr
			!yk = u + A/2.0*yk, so Dgemv and a Daxpy
			!stat = cublasDgemv_v2(cb_h, CUBLAS_OP_N, NX, NX, 0.5_fp, a_d, NX, yk_d, 1, 0.0_fp, yk_d, 1)
			!stat = cublasDaxpy_v2(cb_h, NX, 1.0_fp, u_d, 1, yk_d, 1)
			!Alternatively use the stencil method, turns out this is faster :)
			!yk = u + dt/2 * fi(yk, t, dx, beta), fi=beta/(dx^2)*(u(i-1)+u(i+1)-2u(i))
			!$cuf kernel do <<< *, * >>>
			do k=2,NX-1
				yk_d(k) = u_d(k) + dt*beta/(2.0_fp * dx * dx) * (yk_d(k-1) + yk_d(k+1) - 2.0_fp * yk_d(k)) 
			end do
		end do 
		!update step
		!https://docs.nvidia.com/cuda/cublas/index.html#cublas-lt-t-gt-gemv
		!note I have no idea what the inc[x,y] are supposed to be so I just used 1
		!u = u + A*yk
		stat = cublasDgemv_v2(cb_h, CUBLAS_OP_N, NX, NX, 1.0_fp, a_d, NX, yk_d, 1, 1.0_fp, u_d, 1)
		!update total time
		t_f = t_f + dt 
	end do 
	u = u_d

	if (DEBUG == 1) then
		print *, 'end time = ', t
		print *, 'final time = ', t_f
	end if

	end function impmid_driver

end module impmid_m 

program main
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	use impmid_m
	use utility_m 
	implicit none 
	integer :: nt
	real(fp) :: l2_err, tmp(NX), u_ref(NX), start, finish
	real(fp) :: beta, ti, tf, xi, xf, dx, dt
	real(fp) :: xs(NX), uf(NX)
	real(fp), allocatable :: ts(:)
	character(len=32) :: arg
	character(len=32) :: fname
	character(len=32) :: tmp_char

	beta = 1.0_fp
	ti = 0.0_fp
	tf = 0.1_fp
	xi = 0.0_fp
	xf = 1.0_fp 

	call linspace(xi, xf, xs, .TRUE.) !spacial grid contains endpoint
	dx = xs(2) - xs(1)

	call getarg(1, arg)
	read(arg, *)nt 
	allocate(ts(nt))
	call linspace(ti, tf, ts, .FALSE.) !times does not contain endpoint
	dt = ts(2) - ts(1)
	if (DEBUG == 1) then 
		!print *, 'ts = ', ts(:)
		print *, 'dt = ', dt
	end if 

	call cpu_time(start)
	uf = impmid_driver(nt, ts, xs, dx, dt, ti, tf, beta)
	call cpu_time(finish)
	print *, "method duration: ", finish-start

	if (DEBUG == 1) then
	!	print *, "uf: ", uf(:)
	end if

	write(tmp_char, "(I0.6)"), NX
	fname = "../ref_sol/ref_sol_"// trim(tmp_char) //".txt"
	open(unit=2, file=fname)
	read (2,*) u_ref(:) 
	close(2)

	tmp = uf - u_ref
	l2_err = sqrt(dx * dot_product(tmp, tmp))
	print *, "L2 Error with reference: ", l2_err

	deallocate(ts)

end program main