!nvfortran -o rk4.x rk4.f08 -cpp 

#ifndef NX
#define NX 20
#endif
#ifndef DEBUG
#define DEBUG 0
#endif
#ifndef FULL_TYPE
#define FULL_TYPE real64
#endif
#ifndef REDU_TYPE
#define REDU_TYPE real64
#endif

module utility_m
use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
contains

	subroutine linspace(from, to, array, endpoint)
	!thank you: https://stackoverflow.com/a/57211848
	!modified to add endpoint true/false support
	implicit none
    real(fp), intent(in) :: from, to
    real(fp), intent(out) :: array(:)
    logical, intent(in) :: endpoint
    real(fp) :: range
    integer :: n, d, i

    n = size(array)
    if (endpoint) then 
    	d = -1
    else
    	d = 0
    end if 

    range = to - from

    if (n == 0) return

    if (n == 1) then
        array(1) = from
        return
    end if

    do i=1, n
        array(i) = from + range * (i - 1) / (n + d)
    end do
	end subroutine linspace

	subroutine printmatrix(b,n,m)
	!http://math.hawaii.edu/~gautier/math_190_lecture_11.pdf
	integer::n,m
	real(kind=8)::b(n,m) !n = # rows, m = # columns
	do i=1,n; print '(20f6.2)',b(i,1:m); enddo
	end subroutine

end module utility_m

module rk4_m
contains	

	subroutine exact_solution(x, t, beta, uf)
	!NOTE THIS IS STILL FOR 1D. I haven't updated this
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: x(NX), t, beta
	real(fp), intent(out) :: uf(NX)
	real(fp) :: pi = 4.0*atan(1.0) !supposedly this is pi: https://stackoverflow.com/questions/2157920/why-define-pi-4atan1-d0
	integer :: i 

	do i=1,NX
		uf(i) = exp(-4.0_fp * beta * t * pi) * sin(2.0_fp * pi * x(i))
	end do

	end subroutine exact_solution

	subroutine init_u(u)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(fp), intent(inout) :: u(NX, NX)
	integer :: i, j
	real(fp) :: pi = 4.0*atan(1.0) !supposedly this is pi: https://stackoverflow.com/questions/2157920/why-define-pi-4atan1-d0
	!real(fp) :: pi = 3.14159
	u = 0.0
	do i=1,NX-1 
		do j=1,NX-1
			u(i, j) = sin(real(i-1, fp)/real(NX-1, fp) * pi) * sin(real(j-1, fp)/real(NX-1, fp) * pi)
		end do 
	end do 

	end subroutine init_u

	function fi_f(u, t, dx, beta) result(fn)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(fp), intent(in) :: u(NX, NX), t, dx, beta 
	real(fp) :: fn(NX, NX), stencel
	integer :: i, j

	fn = 0.0

	do i=2,NX-1
		do j=2,NX-1
			stencel = -4.0*u(i, j) + u(i-1, j) + u(i+1, j) + u(i, j-1) + u(i, j+1)
			fn(i, j) = beta/(dx * dx) * stencel 
		end do
	end do

	end function fi_f

	function fi_r(u, t, dx, beta) result(fn)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none 
	real(rp), intent(in) :: u(NX, NX), t, dx, beta 
	real(rp) :: fn(NX, NX), stencel
	integer :: i, j 

	fn = 0.0

	do i=2,NX-1
		do j=2,NX-1
			stencel = -4.0*u(i, j) + u(i-1, j) + u(i+1, j) + u(i, j-1) + u(i, j+1)
			fn(i, j) = beta/(dx * dx) * stencel 
		end do
	end do

	end function fi_r

	function update_step(u, t, dt, dx, beta) result(unp1)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), value, intent(in) :: t, dt, dx, beta
	real(fp), intent(in) :: u(NX, NX)
	real(fp) :: unp1(NX, NX), tmp(NX, NX), k1(NX, NX), k2(NX, NX), k3(NX, NX), k4(NX, NX)

	k1 =  fi_f(u, t, dx, beta)
	tmp = u + dt/2.0_fp * k1
	k2 = fi_f(tmp, t + dt/2.0_fp, dx, beta)
	tmp = u + dt/2.0_fp * k2
	k3 = fi_f(tmp, t + dt/2.0_fp, dx, beta)
	tmp = u + dt * k3
	k4 = fi_f(tmp, t + dt, dx, beta)
	unp1 = u + (1.0_fp/6.0_fp) * dt * (k1 + 2.0_fp * k2 + 2.0_fp * k3 + k4)

	end function update_step
	
	function rk4_driver(nt, ts, dx, init_dt, ti, tf, beta) result(u)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	use utility_m
	implicit none 
	integer, value, intent(in) :: nt 
	real(fp), intent(in) :: ts(:), dx, init_dt, ti, tf, beta
	real(fp) :: t_f=0.0_fp, dt, t 
	real(fp) :: u(NX, NX)
	integer :: i

	dt = init_dt

	call init_u(u)
	if (DEBUG == 1) then 
		print *, 'u_i = '
		call printmatrix(u, NX, NX)
	end if

	call init_u(u)
	t_f = ti 
	do i=1,size(ts)
		t = ts(i)
		u = update_step(u, t, dt, dx, beta)
		t_f = t_f + dt 
	end do 
	if (DEBUG == 1) then 
		print *, 'u_f = '
		call printmatrix(u, NX, NX)
	end if

	if (DEBUG == 1) then
		print *, 'end time = ', t 
		print *, 'final time = ', t_f
	end if

	end function rk4_driver

end module rk4_m 

program main
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	use rk4_m
	use utility_m 
	implicit none 
	integer :: nt, write_ref
	real(fp) :: beta, ti, tf, xi, xf, dx, dt, l2_err, start, finish
	real(fp) :: xs(NX), uf(NX, NX), u_ref(NX*NX), tmp(NX*NX)
	real(fp), allocatable :: ts(:)
	character(len=32) :: arg
	character(len=32) :: fname
	character(len=32) :: tmp_char

	call getarg(2, arg)
	read(arg, *)write_ref

	beta = 1.0_fp 
	ti = 0.0_fp 
	tf = 0.1_fp 
	xi = 0.0_fp 
	xf = 1.0_fp 

	call linspace(xi, xf, xs, .TRUE.) !spacial grid contains endpoint
	dx = xs(2) - xs(1)

	call getarg(1, arg)
	read(arg, *)nt 
	allocate(ts(nt))
	call linspace(ti, tf, ts, .FALSE.) !times does not contain endpoint
	dt = ts(2) - ts(1)
	if (DEBUG == 1) then 
		!print *, 'ts = ', ts(:)
		print *, 'dt = ', dt
	end if 

	call cpu_time(start)
	uf = rk4_driver(nt, ts, dx, dt, ti, tf, beta)
	call cpu_time(finish)
	print *, "method duration: ", finish-start

	write(tmp_char, "(I0.6)"), NX
	fname = "./ref_sol_"// trim(tmp_char) //".txt"

	if (write_ref == 1) then
		u_ref = reshape(uf, (/NX*NX/))
		open(unit=2, file=fname)
		write(2, *) u_ref(:)
		close(2)
	end if

	open(unit=2, file=fname)
	read (2,*) u_ref(:) 
	close(2)

	tmp = reshape(uf, (/NX*NX/)) - u_ref
	l2_err = sqrt(dx * dot_product(tmp, tmp))
	print *, "L2 Error with reference: ", l2_err

	deallocate(ts)

end program main